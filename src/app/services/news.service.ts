import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  url = 'https://fahim-t-bot-db.herokuapp.com/news';

  constructor(private http: HttpClient) {}

  getAllNews = () => {
    return this.http.get(this.url);
  };
}
