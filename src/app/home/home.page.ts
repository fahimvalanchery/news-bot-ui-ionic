import { Component, OnInit, ViewChild } from '@angular/core';
import { NewsService } from '../services/news.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {
  LoadingController,
  IonInfiniteScroll,
  IonContent
} from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: false })
  infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;

  /**
   * Variable initilaisation / Declaration
   */
  fullNewsArray = [];
  slicedNewsArray = [];
  loading;
  totalItems = 0;
  itemsPerpage = 25;
  isLoadedAllItems = false;
  viewGoToTopBtn = false;

  constructor(
    private newsService: NewsService,
    private iab: InAppBrowser,
    public loadingController: LoadingController,
    private socialSharing: SocialSharing
  ) {}

  async ngOnInit() {
    await this.loader();
    await this.getall();
  }

  /**
   * Loading Controller Function
   */
  async loader() {
    this.loading = await this.loadingController.create({
      message: 'Loading...',
      spinner: 'bubbles'
    });
    await this.loading.present();
  }

  /**
   * Api Call For Getting all data
   */
  async getall() {
    await this.newsService.getAllNews().subscribe(
      res => {
        this.fullNewsArray = Object.values(res);
        this.fullNewsArray = this.fullNewsArray.reverse();
        this.totalItems = this.fullNewsArray.length;
        this.slicedNewsArray = this.fullNewsArray.slice(0, this.itemsPerpage);
        this.loading.dismiss();
      },
      err => {
        this.loading.dismiss();
        console.log(err);
        alert('Oops, Something Went Wrong...!');
      }
    );
  }

  /**
   * In-app Browser
   * @param url URL
   * url passes to the browser
   *
   */
  openBlank(url) {
    this.iab.create(url, `_blank`, {
      hideurlbar: 'yes',
      zoom: 'no',
      closebuttoncaption: 'Close'
    });
  }

  /**
   * Infinite-scroll Loading Data function
   * @param e  Event
   *
   */

  async loadData(e) {
    if (this.itemsPerpage < this.totalItems) {
      this.itemsPerpage += this.itemsPerpage;
    } else {
      this.isLoadedAllItems = true;
    }
    setTimeout(() => {
      this.slicedNewsArray = this.fullNewsArray.slice(0, this.itemsPerpage);
      e.target.complete();
      this.viewGoToTopBtn = true;
    }, 1000);
  }

  /**
   * Refresh For Updated data
   * @param e  Event
   */
  doRefresh(e) {
    setTimeout(() => {
      this.getall();
      e.target.complete();
    }, 2000);
  }

  /**
   * Scroll To Top for Button
   */
  scrollToTop() {
    this.content.scrollToTop(1500);
    this.viewGoToTopBtn = false;
  }

  /**
   *  Share Button Function
   * @param news
   * @param e
   *
   * news is whole object to share
   *
   */
  shareBtn(news, e) {
    console.log('share');
    this.socialSharing
      .share(news.heading + '#MyNewsApp', 'My News', null, news.url)
      .then(res => {
        console.log(res);
        // Success!
      })
      .catch(err => {
        console.log(err);
        // Error!
      });
    e.preventDefault(), e.stopPropagation();
  }
}
